import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PedidoDTO } from '../models/pedido.dto';
import { ItemDTO } from '../models/item.dto'

@Injectable({
  providedIn: 'root'
})
export class PedidoService {
  private baseUrl = 'http://localhost:8080/pedidos';

  constructor(private http: HttpClient) { }

  getPedidos(): Observable<PedidoDTO[]> {
    return this.http.get<PedidoDTO[]>(`${this.baseUrl}/listar`);
  }

  getPedidoById(id: number): Observable<PedidoDTO> {
    return this.http.get<PedidoDTO>(`${this.baseUrl}/buscar/${id}`);
  }

  createPedido(pedido: PedidoDTO): Observable<PedidoDTO> {
    return this.http.post<PedidoDTO>(`${this.baseUrl}/add`, pedido);
  }

  updatePedido(id: number, pedido: PedidoDTO): Observable<PedidoDTO> {
    return this.http.put<PedidoDTO>(`${this.baseUrl}/alterar/${id}`, pedido);
  }

  deletePedido(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/delete/${id}`);
  }

  addItemToPedido(id: number, item: ItemDTO): Observable<PedidoDTO> {
    return this.http.post<PedidoDTO>(`${this.baseUrl}/${id}/addItem`, item);
  }
}
