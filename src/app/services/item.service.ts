import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ItemDTO } from '../models/item.dto'

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private baseUrl = 'http://localhost:8080/itens';

  constructor(private http: HttpClient) { }

  getItens(): Observable<ItemDTO[]> {
    return this.http.get<ItemDTO[]>(`${this.baseUrl}/listar`);
  }


  createItem(item: ItemDTO): Observable<ItemDTO> {
    return this.http.post<ItemDTO>(`${this.baseUrl}/criar`, item);
  }

  updateItem(id: number, item: ItemDTO): Observable<ItemDTO> {
    return this.http.put<ItemDTO>(`${this.baseUrl}/atualizar/${id}`, item);
  }

  deleteItem(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/remover/${id}`);
  }

  getItemById(id: number): Observable<ItemDTO> {
    return this.http.get<ItemDTO>(`${this.baseUrl}/buscar/id/${id}`);
  }

  getItemByDescricao(descricao: string): Observable<ItemDTO> {
    return this.http.get<ItemDTO>(`${this.baseUrl}/buscarbyDescricao?descricao=${descricao}`);
  }
}
