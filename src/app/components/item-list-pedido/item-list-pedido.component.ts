import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from '../../services/item.service';
import { PedidoService } from '../../services/pedido.service';
import { ItemDTO } from '../../models/item.dto';
import { PedidoDTO } from '../../models/pedido.dto';

@Component({
  selector: 'app-item-list-pedido',
  templateUrl: './item-list-pedido.component.html',
  styleUrls: ['./item-list-pedido.component.css']
})
export class ItemListPedidoComponent implements OnInit {
  itens: ItemDTO[] = [];
  searchQuery: string = '';
  pedido: PedidoDTO;
  valorFinal: number = 0;
  valorImposto: number = 0;

  constructor(
    private itemService: ItemService,
    private pedidoService: PedidoService,
    private router: Router
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state as { pedido: PedidoDTO };
    this.pedido = state?.pedido || { id: 0, clientName: '', tipo: '', cnpjOuCpf: '', dataCompra: '', valorTotal: 0, itens: [] };
  }

  ngOnInit(): void {
    this.loadItens();
  }

  loadItens(): void {
    this.itemService.getItens().subscribe((data: ItemDTO[]) => {
      this.itens = data;
    });
  }

  addItem(item: ItemDTO): void {
    this.pedido.itens.push(item);
    this.updateValorFinal();
  }

  removeItem(item: ItemDTO): void {
    this.pedido.itens = this.pedido.itens.filter(i => i.idProduto !== item.idProduto);
    this.updateValorFinal();
  }

  updateValorFinal(): void {
    this.valorFinal = this.pedido.itens.reduce((total, item) => total + (item.valor * item.quantidade), 0);
    if (this.pedido.tipo === 'CPF') {
      this.valorImposto = this.valorFinal * 0.052;
    } else if (this.pedido.tipo === 'CNPJ') {
      this.valorImposto = this.valorFinal * 0.032;
    }
    this.valorFinal += this.valorImposto;
  }

  searchItems(): void {
    const query = this.searchQuery.trim();
    if (!query) {
      this.loadItens();
      return;
    }

    const isNumeric = !isNaN(Number(query));
    if (isNumeric) {
      this.itemService.getItemById(Number(query)).subscribe(
        (data: ItemDTO) => {
          this.itens = [data];
        },
        () => {
          this.itens = [];
        }
      );
    } else {
      this.itemService.getItemByDescricao(query).subscribe(
        (data: ItemDTO) => {
          this.itens = [data]; // Ajuste aqui para lidar com um único ItemDTO
        },
        () => {
          this.itens = [];
        }
      );
    }
  }

  confirmPedido(): void {
    this.pedido.valorTotal = this.valorFinal;
    this.pedidoService.createPedido(this.pedido).subscribe(() => {
      this.router.navigate(['/pedidos']);
    });
  }
}
