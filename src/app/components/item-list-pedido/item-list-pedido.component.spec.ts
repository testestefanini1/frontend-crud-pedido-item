import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListPedidoComponent } from './item-list-pedido.component';

describe('ItemListPedidoComponent', () => {
  let component: ItemListPedidoComponent;
  let fixture: ComponentFixture<ItemListPedidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemListPedidoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemListPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
