import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { ItemDTO } from '../../models/item.dto';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  itens: ItemDTO[] = [];
  searchQuery: string = '';

  constructor(private itemService: ItemService) { }

  ngOnInit(): void {
    this.loadItens();
  }

  loadItens(): void {
    this.itemService.getItens().subscribe((data: ItemDTO[]) => {
      this.itens = data;
    });
  }

  deleteItem(idProduto: number): void {
    this.itemService.deleteItem(idProduto).subscribe(() => {
      this.itens = this.itens.filter(item => item.idProduto !== idProduto);
    });
  }

  searchItems(): void {
    const query = this.searchQuery.trim();
    if (!query) {
      this.loadItens();
      return;
    }

    const isNumeric = !isNaN(Number(query));
    if (isNumeric) {
      this.itemService.getItemById(Number(query)).subscribe(
        (data: ItemDTO) => {
          this.itens = [data];
        },
        () => {
          this.itens = [];
        }
      );
    } else {
      this.itemService.getItemByDescricao(query).subscribe(
        (data: ItemDTO) => {
          this.itens = [data];
        },
        () => {
          this.itens = [];
        }
      );
    }
  }

}
