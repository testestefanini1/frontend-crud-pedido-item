import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PedidoService } from '../../services/pedido.service';
import { PedidoDTO } from '../../models/pedido.dto';
import { ItemDTO } from '../../models/item.dto';

@Component({
  selector: 'app-pedido-form',
  templateUrl: './pedido-form.component.html',
  styleUrls: ['./pedido-form.component.css']
})
export class PedidoFormComponent implements OnInit {
  pedido: PedidoDTO = {
    id: 0,
    clientName: '',
    tipo: '',
    cnpjOuCpf: '',
    dataCompra: '',
    valorTotal: 0,
    itens: []
  };

  constructor(
    private pedidoService: PedidoService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras.state as { pedido: PedidoDTO };
    if (state && state.pedido) {
      this.pedido = state.pedido;
    }
  }

  onSubmit(): void {
    this.pedidoService.createPedido(this.pedido).subscribe(() => {
      this.router.navigate(['/pedidos']);
    });
  }

  openItemList(): void {
    this.router.navigate(['/item-list-pedido'], {
      state: { pedido: this.pedido }
    });
  }
}
