import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemService } from '../../services/item.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ItemDTO } from '../../models/item.dto';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {
  itemForm: FormGroup;
  itemId: number | null = null;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.itemForm = this.fb.group({
      descricao: ['', Validators.required],
      quantidade: ['', Validators.required],
      valor: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id) {
        this.itemId = +id;
        this.itemService.getItemById(this.itemId).subscribe(item => {
          this.itemForm.patchValue(item);
        });
      }
    });
  }

  onSubmit(): void {
    if (this.itemForm.valid) {
      const item: ItemDTO = this.itemForm.value;
      if (this.itemId) {
        this.itemService.updateItem(this.itemId, item).subscribe(() => {
          this.router.navigate(['/itens']);
        });
      } else {
        this.itemService.createItem(item).subscribe(() => {
          this.router.navigate(['/itens']);
        });
      }
    }
  }
}
