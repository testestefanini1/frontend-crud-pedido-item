import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PedidoListComponent } from './components/pedido-list/pedido-list.component';
import { PedidoFormComponent } from './components/pedido-form/pedido-form.component';
import { ItemListComponent } from './components/item-list/item-list.component';
import { ItemFormComponent } from './components/item-form/item-form.component';
import { ItemListPedidoComponent } from './components/item-list-pedido/item-list-pedido.component';


const routes: Routes = [
  { path: '', redirectTo: '/pedidos', pathMatch: 'full' },
  { path: 'pedidos', component: PedidoListComponent },
  { path: 'pedido-form', component: PedidoFormComponent },
  { path: 'pedido-form/:id', component: PedidoFormComponent },
  { path: 'itens', component: ItemListComponent },
  { path: 'item-form', component: ItemFormComponent },
  { path: 'item-form/:id', component: ItemFormComponent },
  { path: 'item-list-pedido', component: ItemListPedidoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
