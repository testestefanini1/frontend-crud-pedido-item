import { ItemDTO } from './item.dto';

export interface PedidoDTO {
  id: number;
  clientName: string;
  tipo: string;
  cnpjOuCpf: string;
  dataCompra: string;
  valorTotal: number;
  itens: ItemDTO[];
}
