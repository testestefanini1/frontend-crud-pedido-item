export interface ItemDTO {
  idProduto: number;
  descricao: string;
  quantidade: number;
  valor: number;
}
